### Synopsis

La nouvelle quête de Link prend place quelques générations après *Twilight Princess* and s'étend sur deux continents différents d'Hyrule. Explorez huit temples uniques et terrassez ennemis et boss pour obtenir de puissants objets qui vous aideront à atteindre votre but. Explorez de nombreuses zones d'Hyrule et aidez différentes races à résoudre leurs conflits.

Davantage d'informations sur le [site officiel](https://sites.google.com/site/zeldabom/).

![Book of Mudora](artworks/artwork_book.png "Book of Mudora")
