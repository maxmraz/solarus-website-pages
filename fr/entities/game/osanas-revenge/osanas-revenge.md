***Attention: ce jeu contient du langage grossier.***

Ce jeu est un shooter en vue de dessus, rempli de memes et de private jokes issues du drama de Yandere Simulator et de ses contretemps de développement, en particulier son premier rival (Osana Najimi) retardé de nombreuses années. Cependant, vous n'avez pas besoin d'être familier avec tout cela pour y jouer et l'apprécier.

Ce jeu peut être terminé en moins d'une heure.

### Synopsis

Osana a été explusée de l'école après de nombreuses diffamations envers elle. Cette expérience a été très difficile pour elle car elle a été harcelée pendant des jours.

Plus tard, elle a découvert les magouilles d'Ayano et tout est devenu clair comme de l'eau de roche. Le seul et unique coupable doit être puni de ses propres mains ! Mais elle n'était pas prête pour combattre... Elle a commencé à s'entraîner à la fois physiquement et mentalement avant d'affronter à nouveau son rival. De nombreux mois ont passé...

Le jour où elle a décidé d'affronter à nouveau Ayano, une horde effrontée de Gremlins a envahi sa ville natale. Mais elle aura sa vengeance coûte que coûte. Rien de l'arrêtera. Pas même une armée de Gremlins !
