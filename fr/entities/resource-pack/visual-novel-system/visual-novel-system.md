### Présentation

*Visual Novel System* est une refonte du système de dialogues de Solarus. Elle permet de personnaliser rapidement et facilement tous les aspects de vos dialogues.

- Personnaliser les boites de dialogues, les noms, les personnages, les arrières-plans, et même les effets sonores !
- Ajustez vos configurations selon le personnage, la scène, ou même le langage utilisé.
- Modifiez l'apparence du text à volonté, de la taille à la couleur, en passant par la police de caractère... et même au milieu d'une ligne !
- Insérez des images et des sprites animés au milieu de vos lignes de texte.
