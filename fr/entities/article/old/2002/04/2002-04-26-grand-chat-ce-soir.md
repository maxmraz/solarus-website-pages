<p>Salut à tous !</p>

<p>D'abord je vous dis un grand merci car vous êtes très nombreux à avoir déjà téléchargé le jeu !</p>

<p>Ce soir à 20 heures (heure française), nous organisons un grand chat pour parler de Zelda Solarus ! Vous pouvez accéder au chat en cliquant sur le lien du menu de gauche.</p>

<p>Soyez nombreux ce soir !</p>