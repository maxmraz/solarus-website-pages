<p>Vendredi 19 Avril -&gt; Vendredi 26 Avril !</p>

<p>A sept jours de la sortie du jeu complet, nous sommes dans les derniers préparatifs. Netgamer doit encore faire une image pour l'écran "the end" et devra ensuite compiler le jeu. Il prépare également une notice au format HLP, qui sera en téléchargement avec le jeu. Nous pensons également la mettre en ligne sur le site.</p>

<p>De mon côté, j'ai corrigé de nombreux bugs. Par exemple, l'animation de l'épée qui s'affichait au-dessus des éléments graphiques censés masquer Link (comme les plate-formes ou certaines portes).</p>

<p>La durée de vie du jeu est difficile à estimer. En effet, il faut savoir que le chronomètre de jeu, disponible dans le menu, n'est pas fiable car il s'arrête pendant les dialogues et certaines séquences, et surtout parce qu'il ne chronomètre que le jeu sauvegardé (il oublie donc les game over). Bref, le temps affiché est très inférieur au vrai temps de jeu. J'estime très grossièrement que la durée de vie doit se situer entre 25 et 40 heures, mais rien n'a été calculé précisément. Et puis tout dépend de vos souvenirs à Zelda 3 et de votre expérience de joueur, même si le jeu est accessible à tous, grâce à la simplicité des touches et à la notice détaillée.</p>

<p>Si vous voulez des chiffres, sachez qu'il m'a fallu 6 heures pour refaire tout le jeu (une fois de plus), le connaissant évidemment par coeur. Un de nos testeurs (qui se reconnaîtra), et qui a recommencé sa sauvegarde alors qu'il était arrivé à la fin du niveau 7 (il connaissait donc déjà une bonne partie du jeu, je rappelle qu'il y a 9 donjons), a mis environ 12 heures. J'espère que vous passerez de nombreuses heures sur ZS !</p>

<p>Après la sortie du jeu, le site continuera à vivre car nous vous préparons une surprise... Vous devez déjà avoir une idée de ce que c'est mais je n'en dis pas plus !</p>