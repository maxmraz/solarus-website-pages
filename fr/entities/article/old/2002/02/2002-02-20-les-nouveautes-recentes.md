<p>Salut à tous !</p>

<p>Netgamer ne m'a toujours pas envoyé l'intégralité du niveau 8 (et encore moins du 9) mais ce n'est pas bien grave car maintenant que le chemin entre les niveaux 7 et 8 est fini, je fais d'autres trucs en attendant. Et ce n'est vraiment pas du temps perdu, car voici les dernières améliorations que j'ai effectuées ces derniers jours :</p>

<p><ul type=disc>
<li>Les graphismes d'intérieur des maisons ont été entièrement refaits car l'ancien chipset n'était pas du tout optimisé et les couleurs avaient trop de contraste. Maintenant il y a beaucoup plus de décors dans les maisons.</li>
<li>La tondeuse, jugée "bidon" par certains d'entre vous, a été remplacée par un autre objet, qui existe dans les épisodes de Zelda. De même, quelques petits changements divers ont été effectués au début du jeu, comme le code secret du premier donjon, qui est maintenant aléatoire et que vous devez taper manuellement pour pouvoir entrer.</li>
<li>Les décors du niveau 2 ont été refaits totalement ! J'ai remplacé le chipset de la grotte par celui d'un vrai donjon.</li>
<li>Mais j'ai gardé le meilleur pour la fin : désormais les rubis et les coeurs sont affichés en permanence à l'écran. Plus besoin de mettre pause pour connaître votre état de santé et de richesse !</li>
</ul></p>

<p>Ne vous inquiétez pas, vous aurez bientôt droit à quelques screenshots de tout ceci...</p>