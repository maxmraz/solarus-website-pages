Bonjour à toutes et à tous,

Le développement de notre projet <a href="http://www.zelda-solarus.com/zs/article/zmc-presentation/">Zelda Mercuris' Chest</a> continue !

Avec Solarus Quest Editor, Newlink se met au mapping pour notre plus grand bonheur.

Voici quelques images de la principale ville du jeu :

<a href="../data/fr/entities/article/old/2016/05/images/town_4.png"><img class="aligncenter size-medium wp-image-38051" src="/images/town_4-300x225.png" alt="town_4" width="300" height="225" /></a>

<a href="../data/fr/entities/article/old/2016/05/images/town_3.png"><img class="aligncenter size-medium wp-image-38052" src="/images/town_3-300x225.png" alt="town_3" width="300" height="225" /></a>

<a href="../data/fr/entities/article/old/2016/05/images/town_2.png"><img class="aligncenter size-medium wp-image-38053" src="/images/town_2-300x225.png" alt="town_2" width="300" height="225" /></a>

&nbsp;

Vous l'aurez peut-être remarqué, le graphisme de Link a été personnalisé ! Merci à Newlink qui n'a jamais aussi bien porté son nom :)

Il n'y a pas encore les autres personnages de la cité, mais ce sera une ville animée et vaste. Il y aura beaucoup de mini-quêtes !

Newlink et moi avons aussi réalisé une forêt un peu mystérieuse. Je ne vous en dis pas plus, voici plutôt les images :

<a href="../data/fr/entities/article/old/2016/05/images/torin_forest_2.png"><img class="aligncenter size-medium wp-image-38049" src="/images/torin_forest_2-300x225.png" alt="torin_forest_2" width="300" height="225" /></a>

<img class="aligncenter size-medium wp-image-38050" src="/images/torin_forest_1-300x225.png" alt="torin_forest_1" width="300" height="225" />

On travaille donc actuellement sur le monde extérieur. Newlink a beaucoup d'idées et chaque région aura son propre thème visuel. Il y a encore beaucoup de boulot mais cette fois-ci ça avance pour de bon !