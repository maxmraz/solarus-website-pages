Une nouvelle rubrique vient de faire son apparition : les musiques originales (OST) de Zelda Mystery of Solarus DX !

Cinq compositions inédites ont été créées spécialement pour le jeu par Marine et Metallizer. De la Montagne des Terreurs à la Tour des Cieux en passant par l'inoubliable Temple des Souvenirs, vous pouvez les écouter et les les réécouter à volonté !
<ul>
	<li><span style="line-height: 13px;"><a href="http://www.zelda-solarus.com/zs/article/zmosdx-musiques/">Musiques originales</a>
</span></li>
</ul>