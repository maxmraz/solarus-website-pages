Bonsoir à toutes et à tous :)

C'est avec plaisir que je poste ce soir ma première news sur le nouveau site. Je tiens dans un premier temps à remercier Christopho pour la confiance qu'il m'a accordé pour le développement du site. C'était un vrai plaisir de le réaliser.
Merci aussi à toutes les personnes qui de près ou de loin m'ont aidé à élaborer ce projet.
J'espère que ce site vous plait et qu'il permettra de vous apporter pleins de choses :)
J'ai passé ces derniers jours à corriger pas mal de bugs en tout genre et de nouvelles améliorations devraient voir le jour ces prochaines semaines et ces prochains mois.

En attendant, le basculement du contenu continue encore et encore !
En effet, <a href="http://www.zelda-solarus.com/zs/article/zalttp-soluce/">la soluce de A Link To The Past</a> est à présent complètement publiée. Merci encore Renkineko pour son travail remarquable.
De plus, il a aussi commencé à déplacer <a href="http://www.zelda-solarus.com/zs/article/ztaol-soluce/">la soluce de The Adventure Of Link</a>, illustrée elle aussi.

De son coté, Valoo a presque terminé de migrer<a href="http://www.zelda-solarus.com/zs/article/zoot-soluce/"> la solution de Ocarina Of Time (Master Quest)</a>.

Enfin, Morwenn est en train de rédiger la fin de <a href="http://www.zelda-solarus.com/zs/article/zla-soluce/">la solution de Link's Awakening</a>. <a href="http://www.zelda-solarus.com/zs/soluce/zla-soluce-chapitre-12-temple-du-masque-niveau-6/">Le temple du masque (partie 12)</a> est déjà en ligne !

&nbsp;

<em>Pendant ce temps, le printemps s'installe tout doucement et  le peuple Mojo attend l'E3 avec impatience ...</em>