<p>Le quatrième donjon est en pleine phase de création ! Toutes les salles sont déjà dessinées, il ne me reste plus que le plus dur : rajouter les événements.</p>

<p>Bonne nouvelle : Thomas est (enfin !) rentré de vacances et il a promis de s'attaquer à un nouveau chipset dès le week-end prochain ! D'ici là j'espère avoir terminé le quatrième donjon pour pouvoir avancer le plus tôt possible dans la Carte du Monde.</p>

<p>Au fait, ça n'a rien à voir avec tout ceci mais si vous avez téléchargé la démo, je vous rappelle que vous devez impérativement avoir les <a href="http://www.zelda-solarus.com/download.php3?name=polices_icone">polices&nbsp;d'écran&nbsp;françaises</a> pour obtenir des dialogues normaux (avec les accents). Je tenais à vous le signaler car vous n'avez pas été très nombreux à les télécharger par rapport à la démo.</p>

<p>Vous aurez droit à quelques nouveaux screenshots dans les jours à venir...</p>