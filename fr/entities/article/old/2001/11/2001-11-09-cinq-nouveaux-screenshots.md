<p>Salut à tous !</p>

<p>Comme promis voici les screenshots du sixième donjon ! Ce donjon est encore plus vaste, encore plus long et encore plus dur que les autres. Une des nouveautés sera qu'il est beaucoup moins linéaire que les autres. Ce qui donnera lieu à de nouvelles énigmes, en particulier avec les planchers-interrupteurs... Regardez plutôt les screenshots :</p>

<table border="0" cellpadding="15" cellspacing="0" align="center">
<tr align="center">
<td><a href="/images/solarus-ecran44.png" target="_blank"><img src="/images/solarus-ecran44.png" width="150" border="0"></a></td>
<td><a href="/images/solarus-ecran45.png" target="_blank"><img src="/images/solarus-ecran45.png" width="150" border="0"></a></td>
</tr>
<tr>
<td><a href="/images/solarus-ecran46.png" target="_blank"><img src="/images/solarus-ecran46.png" width="150" border="0"></a></td>
<td><a href="/images/solarus-ecran47.png" target="_blank"><img src="/images/solarus-ecran47.png" width="150" border="0"></a></td>
</tr>
<tr>
<td colspan=2><a href="/images/solarus-ecran48.png" target="_blank"><img src="/images/solarus-ecran48.png" width="150" border="0"></a></td>
</tr>
</table>