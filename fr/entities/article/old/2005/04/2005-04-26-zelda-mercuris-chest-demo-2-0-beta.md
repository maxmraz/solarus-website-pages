<p>Nous sommes encore le 26 avril, et comme promis voici la fameuse démo tant attendue !</p>

<p>Il s'agit d'une version bêta parce que nous n'avons pas encore eu le temps de la tester vraiment à fond et parce que nous comptons sur vous pour nous signaler des bugs. Il semblerait notamment que jouer avec une manette ne soit pas encore possible. En tout cas, si vous trouvez des bugs, n'hésitez pas à m'en informer :)</p>

<p>Il est probable que la démo soit réactualisée dans les prochaines semaines au fur et à mesure des corrections de bugs, jusqu'à ce que l'on aboutisse à une version stable et définitive.</p>

<p>J'espère en tout cas que la démo vous plaira !</p>

<p>Update : et voilà, après quelques minutes de retard, la démo est vraiment disponible ! Amusez-vous bien !</p>

<p><a href="http://www.zelda-solarus.com/download.php?name=zmcdemo">Télécharger la démo</a></p>