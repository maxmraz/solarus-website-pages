Comme promis, chaque semaine on va vous donner une capture d'écran de Zelda Mercuris' Chest par semaine (au moins !).

Voici un travail en cours par Metallizer sur l'un des donjons du jeu :

<a href="../data/fr/entities/article/old/2017/01/images/zmc_sacred_crater_1.png"><img class="aligncenter size-medium wp-image-38071" src="/images/zmc_sacred_crater_1-300x225.png" alt="zmc_sacred_crater_1" width="300" height="225" /></a>

Tout ceci est en travaux et risque encore d'évoluer mais cela vous donne un petit aperçu ! Ce qui est sûr, c'est qu'il va y avoir de l'épique dans ce donjon.

&nbsp;