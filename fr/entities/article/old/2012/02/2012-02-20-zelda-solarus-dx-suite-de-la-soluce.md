Bonjour à tous,

Deux nouveaux épisodes de la soluce vidéo sont disponibles. La série va maintenant jusqu'à la fin du deuxième donjon.
Ces deux épisodes sont commentés par Mymy et moi-même :).

[list]
[li] [url=http://www.youtube.com/watch?v=PO-issFtjgY]Épisode 3 : du niveau 1 au niveau 2[/url][/li]
[li] [url=http://www.youtube.com/watch?v=PsEtixtmQ8s]Épisode 4 : niveau 2 - Caverne de Roc[/url][/li]
[/list]

Même si vous n'êtes pas bloqués dans le jeu, ces vidéos ont l'air de vous plaire ^^. Nous allons bien sûr continuer la série, le tournage des prochains épisodes étant prévu pour cette semaine.