Après plusieurs semaines d'attente, je suis heureux de vous annoncer que la version anglaise de [url=http://www.zelda-solarus.com/jeu-zsdx-download]Zelda Mystery of Solarus DX[/url] est disponible !

Un grand merci à Rypervenche, AleX_XelA, Jeff pour leur travail colossal et à la communauté [url=http://openpandora.org]OpenPandora[/url].

Zelda Mystery of Solarus DX passe donc en version 1.5.0. Voici la liste complète des changements de cette version :
[list]
[li]Traduction anglaise disponible[/li]
[li]Correction d'un plantage possible avec les téléporteurs[/li]
[li]Corrections mineures dans le donjon 5[/li]
[/list]

Du côté de [url=http://www.zelda-solarus.com/jeu-zsxd-download]Zelda Mystery of Solarus XD[/url], qui était déjà disponible en anglais, le jeu a lui aussi été mis à jour (version 1.5.2) pour des corrections de bugs.

Au final, nos deux jeux sont donc disponibles en deux langues, français et anglais. La traduction en allemand de Zelda Solarus DX est également en cours de réalisation.
Le choix de la langue s'effectue lors du premier lancement du jeu, et peut ensuite être changé dans l'écran des options avant de lancer une sauvegarde.

N'hésitez pas à transmettre la nouvelle autour de vous ! :)