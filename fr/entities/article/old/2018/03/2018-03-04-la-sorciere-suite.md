Suite aux différents remarques sur la hutte de la sorcière (cf news précédente), nous avons décidé de la modifier au niveau des pots. A présent, ces derniers ressemblent plus à ceux du jeu original. J'espère que cela vous plait. :)

<a href="../data/fr/entities/article/old/2018/03/images/1.png"><img class="alignnone size-medium wp-image-38301" src="/images/1-300x240.png" alt="" width="300" height="240" /></a>

Sinon, on avance beaucoup au niveau des décors du jeu. Cela va vraiment envoyer du lourd ^_^
Enfin, sachez qu'une game jam a lieu bientôt afin d'avancer un maximum sur le projet. On espère toujours une date de sortie pour cette année !