Voici une petite capture d'écran de la dernière nouveauté en date. :)
Comme annoncé dans la nouvelle précédente, il s'agit d'un écran d'options accessible depuis le menu des sauvegardes, et qui permet de régler plusieurs paramètres globaux à toutes les sauvegardes : la langue, la résolution et le volume de la musique et des sons.

[center][img]http://www.zelda-solarus.com/images/zsdx/global_options.png[/img][/center]
Concernant la langue, je rappelle qu'il n'y aura plus qu'un fichier à télécharger pour l'ensemble des langues du jeu (au lieu d'avoir des installeurs ou paquets différents pour chaque langue comme actuellement). Au premier lancement du jeu, un écran vous propose de choisir la langue. Cette langue est alors mémorisée pour les lancements suivants, et peut être changée ultérieurement grâce à l'écran d'options ci-dessus.

Le mode d'affichage est maintenant lui aussi configurable depuis cet écran, en plus de l'être au cours d'une partie depuis le sous-écran de pause (qui lui ne change pas). La touche F5 permet également de changer de mode d'affichage à n'importe quel moment.

Enfin, la personnalisation du volume de la musique et des sons est une nouveauté puisque jusqu'à présent, on ne pouvait pas les modifier ni même les désactiver. Bien que les musiques et les sons soient selon moi indispensables à l'ambiance du jeu, certains joueurs ne sont pas de cet avis et peuvent vouloir par exemple désactiver la musique.

A part ça, à la demande générale, je suis en train de travailler sur l'animation de Link qui monte ou descend des escaliers avec le petit son caractéristique, le genre de petit détail que les fans remarquent et qui ajoute une touche beaucoup plus soignée au jeu. :P