A la demande de Nintendo, je suis contraint de fermer le site. A 11h53 ce matin, Nintendo a envoyé un mail à notre hébergeur, expliquant que notre site violait les droits d'auteurs de Zelda en faisant le projet Zelda : Mercuris' Chest. Nintendo craint que notre projet leur fasse trop de concurrence en 2023. Notre hébergeur nous a donc demandé de stopper immédiatement nos activités relatives à Zelda, et le site sera totalement supprimé dans un moins de 24 heures. Je n'ai donc pas d'autres choix que de fermer le site. Cette page restera disponible jusqu'à la fin, afin que vous soyez au courant de la situation. Le forum restera lui aussi ouvert pendant cette durée.

[url=http://forums.zelda-solarus.com]Visiter les forums[/url]

En tout cas, j'aimerais souligner que ces longues années à développer ces deux Zelda amateurs et à enrichir la communauté Zelda francophone furent très enrichissantes et c'est avec la larme à l'oeil que je suis obligé de mettre fin à cette aventure. Dommage que cela se termine comme ça, mais finalement c'est pour le bien de Nintendo... ? A bientôt dans une autre vie peut etre...

Christopho et toute l'équipe ZS.