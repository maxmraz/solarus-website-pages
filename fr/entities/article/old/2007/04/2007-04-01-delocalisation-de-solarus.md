Il me reste peu de temps avant que Christopho me bannisse. J'écris cette news rapidement afin de vous communiquer une décision prise par le CAM (Conseil des Anciens Membres). Les membres qui ont plus de 2500 messages et qui ne sont pas modos ont en effet un forum privé que Chris avait inauguré et dont il avait oublié l'existence. Heureusement, ça se sera retourné contre lui.

Bref, depuis ce matin la CAM, dont je suis le président, débattons pour savoir comment il faut réagir fasse aux poussées de Christopho. Nous avons essayé de le raisonner par plusieurs fois, rien ne fait: il nous ignore.

Car les faits sont là: Christopho s'est servi de son équipe comme d'un vulgaire jouet. C'est sûr, c'est plus facile de faire modérer les autres et de prendre toute la gloire. Malheureusement Chris, nous aurons le dernier mot.

Aussi, nous avons pris la décision de faire migrer le forum. Un de nos espions dans l'équipe d'administration se chargera de récupérer la base de données afin de ne pas perdre de messages. Ces forums seront identiques aux anciens, la seule différence viendra du fait que les modérateurs corrompus et Christopho ne seront inscrit. C'est dur, mais c'est le seul moyen de sortir de cette tyrannie infâme. Mes amis, suivez nous: la liberté est proche.

En attendant d'avoir un serveur à nous, l'adresse du forum est la suivante:

http://zelda-solarus.superforum.fr/

D'ici quelques jours, nous aurons récupéré la base de données et tout sera rentré en ordre.

Ce sont des temps durs mes amis; mais je ne doute pas que nous en sortirons victorieux. Je sais que je vais me faire bannir pour ça, mais le jeu en vaut la chandelle.

Merci de croire en nous,

le Conseil des Anciens Membres

P.S.: Chris, fils de moblin putride, sache que j'ai entamé des procédures judiciaires contre toi, pour exploitation de personnel. L'affaire est classé sous le numéro 010407 au palais de justice. Que ta descendance soit maudite !