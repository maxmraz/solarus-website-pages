La solution vidéo de Zelda Mystery of Solarus DX est finie depuis un moment, mais il manquait un épisode spécial dédié au Fragments de Cœur. C'est désormais chose faite !

Dans cette vidéo j'utilise une version de développement avec des touches de triche pour obtenir tous les Fragments de Cœur du jeu à partir d'une nouvelle sauvegarde :)

Rappelons également que la liste des Fragments de Cœur est aussi disponible en version textuelle dans la solution de Renkineko, et aussi dans la solution en PDF (par Neovyse et Renkineko) !
<ul>
	<li><a href="http://www.youtube.com/watch?v=VOXOQLCExrY]">Vidéo bonus : Les 32 Fragments de Cœur</a></li>
	<li><a href="http://www.zelda-solarus.com/zs/soluce/zmosdx-soluce-quete-annexe-les-fragments-de-coeur/">Version textuelle : Les 32 Fragments de Cœur</a></li>
	<li><a href="http://www.zelda-solarus.com/zs/wp-content/uploads/2013/06/Solution%20Zelda%20Solarus%20DX.pdf">Soluce en PDF</a></li>
</ul>
Si vous n'aviez pas tout trouvé, c'est l'occasion d'obtenir ce vous avez manqué !