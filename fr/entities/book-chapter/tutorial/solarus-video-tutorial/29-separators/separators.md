# {title}

[youtube id="Yj-dMFqiF98"]

## Sommaire

- Utiliser les entités séparateurs
  - Pièces distinctes avec séparateurs
  - Ajouter un séparateur au milieu d'une grande pièce
  - Grande pièce avec séparateur supprimé
  - Comment corriger les problèmes de défilement des portes quand elles sont trop proches de séparateurs en T
- Evènements des séparateurs
  - L'évènement `on_activating()`
  - L'évènement `on_activated()`

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
