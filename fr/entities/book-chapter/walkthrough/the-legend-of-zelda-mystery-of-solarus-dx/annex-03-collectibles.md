# Objets récupérables

## Flacons Magiques

### 01 - Pâtisserie

Apportez six pommes au pâtissier (25 rubis pour 3 pommes au magasin du village) et il vous récompensera avec ce Flacon.

[Lien vidéo : Pâtisserie (Flacon Magique)](http://www.youtube.com/watch?v=FanqKKhfilk#t=660s)

### 02 - Cascade du Château d'Hyrule

Une fois que vous aurez les Palmes, vous pouvez faire le tour du Château et rentrer sous la cascade centrale au nord. Un coffre contenant le Flacon Magique vous y attend.

[Lien vidéo : Cascade du Château d'Hyrule (Flacon Magique)](http://www.youtube.com/watch?v=x84d8Lh24Mg#t=333s)

### 03 - Hutte de la Sorcière

Lorsque vous avez accès à Inferno, allez dans la Hutte de la Sorcière à droite de celui-ci. Achetez une potion (n'importe laquelle) pour que la sorcière vous offre le troisième Flacon Magique.

[Lien vidéo : Hutte de la Sorcière (Flacon Magique)](http://www.youtube.com/watch?v=jbUnejEjXrs#t=730s)

### 04 - Montagne des Terreurs

Après avoir utilisé le Miroir Mystique sur la cascade dans la Montagne des Terreurs, laissez-vous tomber de la première corniche que vous croisez. Suivez le chemin en tombant dans les trous, et vous arriverez après 2 étages devant le coffre contenant le quatrième et dernier Flacon Magique

[Lien vidéo : Montagne des Terreurs (Flacon Magique)](http://www.youtube.com/watch?v=yMUZTDv7_Vs#t=940s)

## Précis d'Escrime

Pour récupérer l'attaque tornade ultime, allez sur la Montagne des Terreurs (à l'ouest) et passé le premier pont, dirigez-vous vers l'est. Allumez les torches après vous être débarrassés des pierres noires pour faire apparaître la Fée qui vous enseignera l'attaque.

[Lien vidéo : Montagne des Terreurs (Précis d'Escrime)](http://www.youtube.com/watch?v=0mQrcTSgTt8#t=80s)
