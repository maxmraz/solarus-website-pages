[container]

# {title}

## Create with Solarus

So you want to create a game with Solarus? Here is a bit of help to help you setting up your first project.

* **Resources packs** are Solarus quests containing only scripts and assets (tilesets, sprites, music, sounds, etc.), allowing you to begin with pre-made stuff.
* **Tutorials** are a great way to start, explaining step by step how to build classic elements: maps, menus, cutscenes, puzzles, dungeons, enemies, etc.
* **Documentation** for the Solarus Lua API is where you need to go if you have any question about how to use the API.

[space]

[row]
[column]

[button-highlight type="primary" icon="resource_pack" url="development/resource-packs" label="Resource packs" subtitle="Sprites, tilesets and scripts"]

[/column]
[column]

[button-highlight type="primary" icon="owl" url="development/tutorials" label="Tutorials" subtitle="How to make your own game"]

[/column]
[column]

[button-highlight type="primary" icon="book" url="https://www.solarus-games.org/doc/latest" label="Lua API documentation" subtitle="For Quest makers"]

[/column]
[/row]

[space thickness="50"]

## Contribute to Solarus

Solarus is made by amateurs on their free time, so the team would be very glad if you want to help in any way.

* Read **How to contribute** and come talk with us on Discord, weither you are a C++/Lua developer, Solarus Quest maker, artist, musician, donator or anything else.
* **Donations** are possible, if you feel generous. It will be very much appreciated and will go directly to the nonprofit organization!
* **Source code** is hosted on Gitlab. Feel free to take a look.
**Source code documentation** is the place for technical doc about the engine.

[space]

[row]
[column]

[button-highlight type="primary" icon="contribute" url="development/how-to-contribute" label="How to contribute" subtitle="Wanna help?"]

[/column]
[column]

[button-highlight type="primary" icon="gift" url="development/donation" label="Donation" subtitle="Financially help the project"]

[/column]
[column]
[button-highlight type="primary" icon="code" url="https://gitlab.com/solarus-games/" label="Source code" subtitle="On Gitlab"]

[/column]
[/row]

[/container]