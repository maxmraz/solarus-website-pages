
[container layout="small"]

# {title}

[space thickness="30"]

![Solarus Labs logo](images/solarus-labs-logo.png)

[space thickness="30"]

The Solarus project is supported by the **Solarus Labs** nonprofit organization, based in France.

Legally, the organization is an **Association Loi de 1901** according to French law. The goal of this organization is development, promotion and education of free software for video game creation.

All donations will go directly to the organization, and will be reinvested into the project.

## Board

The current board of Solarus Labs is composed by:

* **President:** Christophe Thiéry
* **Treasurer:** Kévin Baumann
* **Vice-Treasurer:** Benjamin Schweitzer
* **Secretary:** Olivier Cléro

[/container]
