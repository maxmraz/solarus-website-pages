# {title}

[youtube id="iRXMIY3ChaM"]

## Summary

- Using platform stairs on multi-level dungeons
- How to avoid problems with jumpers on an inside corner of a platform
- Placing a wall behind platform stairs to block the hero

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
