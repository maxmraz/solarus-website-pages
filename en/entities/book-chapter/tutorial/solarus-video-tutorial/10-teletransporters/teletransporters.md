# {title}

[youtube id="3Z7Hol4j0II"]

## Summary

- Working with destination entities
- Teletransporters within the same map
- Linking maps with a teletransporter
- Linking maps with a scrolling teletransporter
- Setting the world name

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
