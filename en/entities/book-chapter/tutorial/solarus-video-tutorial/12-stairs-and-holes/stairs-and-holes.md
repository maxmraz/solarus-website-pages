# {title}

[youtube id="ZU60nuP2YAk"]

## Summary

- Working with stairs to link maps
- Creating holes (teletransporter) to fall to another map
  - Setting the map floor property

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
