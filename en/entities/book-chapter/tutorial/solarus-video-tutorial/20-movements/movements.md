# {title}

[youtube id="jAsqZ2CjXkI"]

## Summary

- Working with movements
  - The `sol.movement.create(movement_type)` function
  - Apply a random movement to an NPC
  - Apply a target movement to an NPC to follow the hero
  - Apply a path_finding movement to an NPC to follow the hero
  - Apply a straight movement to a surface on the starting menu

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
