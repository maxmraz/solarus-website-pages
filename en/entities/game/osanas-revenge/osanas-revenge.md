***Warning: crude language is to be expected.***

This game is a top down shooter filled with memes and private jokes arisen from Yandere Simulator drama and developpment setbacks, in particular, its first rival (Osana Najimi) being delayed for multiple years. However, you don't have to be familiar with all of this in order to play and enjoy it.

It can be completed in less than one hour.

### Synopsis

Osana was expelled from school after several false accusations against her. This has been a rough experience for her as she was unrightfully harassed for days.

Later, she discovered about Ayano' shenanigans and everything became crystal clear. The one and only culprit has to be punished by her very own hand! But she was not ready for a fight... She started to train both physically and mentally before facing her rival again. Several months passed...

The day she decides to face off against Ayano, a shameless army of hateful Gremlins invades her home city. But she will have her revenge at any cost. Nothing will stop her. Not even an army of Gremlins!
