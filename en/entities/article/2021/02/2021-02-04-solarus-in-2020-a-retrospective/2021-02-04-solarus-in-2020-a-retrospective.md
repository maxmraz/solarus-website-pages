We took advantage of Solarus Labs yearly meeting to make a retrospective of what happened in 2020, and what are the upcoming projects for Solarus in 2021.

## Solarus in 2020

### The good

We created exactly one year ago [Solarus Labs](/en/about/nonprofit-organization), a nonprofit organization to legally support Solarus. So far, it has only served as a legal way to receive donations. We hope we'll be able to organize some events someday, but the pandemic postpones this again and again.

![Solarus Labs logo](images/solarus-labs-logo.png)

Solarus had two releases: [1.6.3](https://gitlab.com/solarus-games/solarus/-/tags/v1.6.3) and [1.6.4](https://gitlab.com/solarus-games/solarus/-/tags/v1.6.4). They brought a better macOS support, better stability, and first and foremost: a free and open-source tileset by Max Mraz, called *Ocean Set*.

![Ocean Set screenshot](images/ocean-set-screenshot.png)

[Solarus goodies](https://shop.spreadshirt.fr/solarus-labs) began as a joke, but we actually made them real. Eventually, we sold some and it helped us pay the server fees.

![Solarus tee-shirt](images/solarus-tee-shirt.png)

Christopho made a video hommage to Bob Ross. He parodies the famous painter while creating a map in Solarus Quest Editor.

[youtube id="JisU7NR18Hw"]

The community is bigger and bigger, and everyday more involved. Solarus got a lot of love from its community:

- [Visual Novel System](/en/development/resource-packs/visual-novel-system): a feature-packed overhaul of Solarus dialog system.
- New [resource packs](/en/development/resource-packs), new Lua scripts.
- Progression on *[Children of Solarus](https://gitlab.com/solarus-games/children-of-solarus)*: the world map is done!

![Children of Solarus world map](images/children-of-solarus-worldmap.png)

Solarus made some buzz for multiple reasons:

- Solarus has been added to [Recalbox](https://www.recalbox.com/).
- [Liège Game Lab](https://www.liegegamelab.uliege.be/): an university uses Solarus for education.

Some Solarus games have quite buzzed (both by Max Mraz):

- *[Yarntown](https://maxatrillionator.itch.io/yarntown)*: a 2D hommage to *Bloodborne*.
- *[Ocean's Heart](https://store.steampowered.com/app/1393750/Oceans_Heart/)*: the first Solarus game to be released on Steam.

[youtube id="jELtHA_VBJ0"]

For all these reasons, we can say Solarus reaches maturity.

### The less good

Some projects have not progressed a lot:

- Text and video tutorials.
- Collaboration with a French institute.
- Android app.
- New quest launcher/browser.
- The Legend of Zelda: Oni-Link Begins.

## Solarus projects for 2021 and later

We always have lots of projects for Solarus. Here is a list of what we would like to achieve in the year to come, and later.

- Solarus 1.6.5:
  - Bug fix for the legendary issue of the `R` key when resizing tiles in the Quest Editor (done!).
  - Other bug fixes.
  - Improvements for *Ocean's Heart*.
  - Top-secret project... (You'll get some news soon!)

- Solarus ~~1.7~~ **2.0**:
  - Multiplayer features (almost done).
  - New Quest Launcher, with much more features and a fancy UI.
  - Android app (still in alpha).
  - Dark theme for the Quest Editor on Windows.
  - New logo.
  - Website refresh.

- Knowledge transfer:
  - More tutorials.
  - New API documentation website.

- Game development:
  - *The Legend of Zelda: Oni-Link Begins*.
  - *Children of Solarus* (number one priority).

- Goodies:
  - Covid-19 mask?
  - New stickers (to come with the new logo).

## Solarus Labs news

In order to give you the best transparency, we decided to open the [Solarus Labs Git repository](https://gitlab.com/solarus-games/solarus-labs) to the public. This is where we store the source for the organization documents: legal status, meeting notes, etc. However, it's in read-only mode: forks and merge requests are blocked. Only the board members are allowed to edit the files.

## Conclusion

Despite the not-so-good circumstances, 2020 has been a great year for Solarus! Thanks for all the community members. 2021 forecasts great things too: the release of *Ocean's Heart* might bring new people to the community.
