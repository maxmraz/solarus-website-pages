Since Solarus 0.9.0, the format of most data files is being changed to be more readable, easier to parse and more homogeneous.

In Solarus 1.0, good progress has been made in this direction, especially with the new format of maps. Three formats of data files remain to be changed: the strings.dat language file, the sprite sheet files and the quest resource list file. I plan to change them all in Solarus 1.1.

And the work is now finished for the quest resource list file (project_db.dat). This file defines the list of resources (maps, tilesets, sprites, musics, sounds, enemies�) and allows the quest editor to show them and edit them. The new format looks like this:
<pre>map{     id = "outside",      description = "Outside World" }
map{     id = "hero_house",   description = "House of the hero" }
map{     id = "shop",         description = "Shop" }
map{     id = "dungeon_1_1f", description = "Dungeon 1 - First floor" }
map{     id = "dungeon_1_2f", description = "Dungeon 1 - Second floor" }

tileset{ id = "overworld",    description = "Overworld" }
tileset{ id = "house",        description = "House" }
tileset{ id = "dungeon",      description = "Dungeon" }

sound{   id = "door_closed",  description = "Door closing" }
sound{   id = "door_open",    description = "Door opening" }
sound{   id = "enemy_hurt",   description = "Enemy hurt" }
sound{   id = "jump",         description = "Jumping" }
sound{   id = "treasure",     description = "Treasure" }

item{    id = "sword",        description = "Sword" }
item{    id = "bow",          description = "Bow" }
item{    id = "arrow",        description = "Arrows (x1 / x5 / x10)" }

enemy{   id = "soldier",      description = "Soldier" }
enemy{   id = "dragon",       description = "Dragon" }</pre>
Note that the quest editor fully supports the modification of this file since Solarus 1.0.2. More information about this�<a href="http://www.solarus-games.org/doc/1.1/quest_resource_file.html">quest resource list file</a>�is available on the documentation pages.

More importantly, I added to the quest editor a system to upgrade automatically your data files when their format changes. Whenever a new version Solarus is released, if the format of data files has changed, the quest editor will upgrade them for you.