Solarus 1.1 now officially supports three new mobile systems!

You have been waiting for a long time for the Android version. Thanks to Sam101, our games are now officially ported to Android. This Android port is in beta-testing though. There are a lot of different phone models so we need your feedback to know if there are problems. Please post any issue<a href="http://forum.solarus-games.org/"> on the forum</a>.

The <a href="http://www.openpandora.org/">Pandora</a> and the <a href="http://www.gcw-zero.com/">GCW-Zero</a> are two open-source consoles that are oriented to homebrew and retrogaming. Their communities are very active and thanks to their work, Solarus now also works on these two systems!

Therefore, the list of (offically) supported systems is now: Windows, Linux, Mac OS X, OpenBSD, Amiga OS 4, Android, Pandora, GCW-Zero.

See the download pages to get the games on the system of your choice.
<ul>
	<li><a title="Download ZSDX" href="http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-dx/">Download Zelda Mystery of Solarus DX 1.7</a></li>
	<li><a title="Download ZSXD" href="http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-xd/">Download Zelda Mystery of Solarus XD 1.7</a></li>
</ul>
We are also thinking of an iOS port with touchscreen gameplay but this will take time. Not only we have to upgrade the engine and the Lua API to handle the mouse and the touchscreens, but we also need to completely rework the gameplay in the case when no physical button is available. So how do we do right now for the Android version? We actually use a trick: the SDL 1.2 port of Android adds virtual buttons to the screen, that are mapped to SDL keyboard keys.