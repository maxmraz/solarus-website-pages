<strong>NB: Clarification at the bottom of the article.</strong>

For several years, we have been developing in great secrecy a project of unprecedented ambition.

This is not a game, nor a game engine or even a game creation software. This time we went further.

<a href="http://www.solarus-games.org/wp-content/uploads/2016/04/logo_solatrix.png"><img class="aligncenter size-medium wp-image-1387" src="/images/logo_solatrix-300x65.png" alt="logo_solatrix" width="300" height="65" /></a>

<span id="result_box" class="" lang="en"><span title="Nous sommes fiers de vous d�voiler officiellement la Solatrix, une console portable r�volutionnaire d�di�e aux jeux utilisant le moteur Solarus ! ">We are proud to officially unveil the <strong>Solatrix</strong>, a revolutionary handheld console dedicated to games built with the Solarus engine!</span></span>

[caption id="attachment_1388" align="aligncenter" width="277"]<a href="http://www.solarus-games.org/wp-content/uploads/2016/04/editor_screenshot.png"><img class="wp-image-1388 size-medium" src="/images/editor_screenshot-277x300.png" alt="editor_screenshot" width="277" height="300" /></a> The Solatrix will allow you to download games created with Solarus Quest Editor, both our team's games and the community games. It is expected to be available for the holidays, although it is unclear what year.[/caption]

&nbsp;

[caption id="attachment_1389" align="aligncenter" width="300"]<a href="http://www.solarus-games.org/wp-content/uploads/2016/04/console1.jpg"><img class="wp-image-1389 size-medium" src="/images/console1-300x225.jpg" alt="console1" width="300" height="225" /></a> Light and handy, the Solatrix has a 3.5-inch backlit screen.[/caption]

&nbsp;

[caption id="attachment_1390" align="aligncenter" width="225"]<a href="http://www.solarus-games.org/wp-content/uploads/2016/04/console2.jpg"><img class="wp-image-1390 size-medium" src="/images/console2-225x300.jpg" alt="console2" width="225" height="300" /></a> The Mercuris Pack will offer the console accompanied of Zelda Mercuris' Chest, our flagship vaporware.[/caption]

&nbsp;

[caption id="attachment_1391" align="aligncenter" width="300"]<a href="http://www.solarus-games.org/wp-content/uploads/2016/04/console3.jpg"><img class="wp-image-1391 size-medium" src="/images/console3-300x225.jpg" alt="console3" width="300" height="225" /></a> Compatible with the most popular controllers on the market, the Solatrix promises hours of play more exciting than ever.[/caption]

&nbsp;

[caption id="attachment_1392" align="aligncenter" width="300"]<a href="http://www.solarus-games.org/wp-content/uploads/2016/04/console4.jpg"><img class="wp-image-1392 size-medium" src="/images/console4-300x225.jpg" alt="console4" width="300" height="225" /></a> Every Solarus Software Team member has worked hard to give life to this project! The plans will be available soon so that you can also build your own Solatrix.[/caption]

&nbsp;

<span id="result_box" class="short_text" lang="en">That's <span class="">hours of fun</span> <span class="">and joy</span> <span class="">ahead!</span></span>
<h3>Edit:</h3>
Obviously, it was an April's Fools. But it is not impossible to build. Actually, it is quite feasible. The console we use to make these pictures is <a href="http://www.thingiverse.com/thing:939901">this one</a>, by Rasmushauschild, so congrats to him for making such a beautiful console. It works with the <a href="http://blog.petrockblock.com/retropie/">RetroPie</a> distribution, which is a distribution specialized in retro-gaming emulation. And as you may know, <a href="https://github.com/RetroPie/RetroPie-Setup/commit/11c6f1bb0dc5c731b34f05415bc745e9dfd21ed7">Solarus works now on RetroPie</a>! The last missing thing is the little "Solatrix" sticker, but you can print it with the logo above. So now you can build your own Solatrix console!

&nbsp;