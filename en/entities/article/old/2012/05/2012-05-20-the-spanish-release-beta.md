A new version of <a title="Zelda Mystery of Solarus DX" href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus DX</a>(version 1.5.1) is now available. It includes a beta version of the Spanish translation!

Changes:
<ul>
	<li>Spanish translation (beta)</li>
	<li>Fix some issues in the English translation</li>
	<li>Skyward Tower: the hero could get stuck in a wall</li>
	<li>Skyward Tower: other minor improvements</li>
	<li>Fix blue flames shot by some bosses and that were stuck sometimes</li>
	<li>Ancient Castle: minor improvements</li>
	<li>Inferno Maze: a puzzle could be skipped</li>
</ul>
<p style="text-align: center;"><a href="http://www.zelda-solarus.com/jeu-zsdx-download&amp;lang=en">Download now</a> on Zelda Solarus</p>
I would like to thank Clow_eriol, Emujioda, LuisCa, Musty, Xadou, Guopich and falvarez for their contribution to the Spanish translation. It still needs more testing and verifications. Please <a title="Contact" href="http://www.solarus-games.org/contact/">contact me</a> if you want to help!